# Part 4: Analysis

## Filtering Data

The first stage of analysis is to filter out **uninformative data such as control probesets and other internal controls** as well as removing genes with low variance, that would be unlikely to pass statistical tests for differential expression, or are expressed uniformly close to background detection levels. The modifiers to nsFilter below tell nsFilter not to remove probesets without Entrez Gene identifiers, or have duplicated Entrez Gene identifiers.

```R

celfiles.filtered <- nsFilter(celfiles.gcrma, require.entrez=FALSE, remove.dupEntrez=FALSE)

```

In order to find which data was thrown away and why we can run:

```R
celfiles.filtered$filter.log
```

You will see something like this:

```
$numLowVar
[1] 27307

$feature.exclude
[1] 62
```

This means From this we conclude 27,307 probesets have been removed for reasons of low variance, and 62 control probesets have been removed as well.



#### [affy Package](http://bioconductor.org/packages/release/bioc/html/affy.html)

![affy](http://bioconductor.org/shields/years-in-bioc/affy.svg)

The affy package is for data analysis and exploration of Affymetrix oligonucleotide array probe level data.  
The software utilities provided with the Affymetrix software suite summarizes the probe set intensities to form one expression measure for each gene. The expression measure is the data available for analysis.

Some of the features are:

- Convert probe level data to expression values
    - Read *CEL* file information
    - Expression measures i.e. *RMA*, *MAS 5.0*, *MBEI*
- Quality Control
    - *MA Plot*
    - *PM* Correct
    - *Histograms*, *Images* & *Boxplots*
    - *RNA Degradation* plots
- Normalization


#### [affyQCReport Package](http://bioconductor.org/packages/release/bioc/html/affyQCReport.html)

This package creates a QC report for an AffyBatch object. The report is intended to allow the user to quickly assess the quality of a set of arrays in an AffyBatch object.


### Finding Differentially Expressed Probesets

There are some packages for analyzing microarray data files like `limma` and `affy` that we will describe them below.
To make the data ready for them following processes are required:

```R

samples <- celfiles.gcrma$Target
samples
samples <- as.factor(samples)
# set up the experimental design
design <- model.matrix(~0 + samples)
colnames(design) <- c("choroid", "huvec", "iris", "retina")

```

then the `design` is

```
choroid huvec iris retina
1        0     0    1      0
2        0     0    0      1
3        0     0    0      1
4        0     0    1      0
5        0     0    0      1
6        0     0    1      0
7        1     0    0      0
8        1     0    0      0
9        1     0    0      0
10       0     1    0      0
11       0     1    0      0
12       0     1    0      0
attr(,"assign")
[1] 1 1 1 1
attr(,"contrasts")
attr(,"contrasts")$samples
[1] "contr.treatment"
```

### Analyzing Data

## Artificial Neural Network

From [Wikipedia](https://en.wikipedia.org/wiki/Artificial_neural_network):

An Artificial neural network (ANN) is a network inspired by biological neural networks (the central nervous systems of animals, in particular the brain) which are used to estimate or approximate functions that can depend on a large number of inputs that are generally unknown.

For example, a neural network for handwriting recognition is defined by a set of input neurons which may be activated by the pixels of an input image. After being weighted and transformed by a function (determined by the network's designer), the activations of these neurons are then passed on to other neurons. This process is repeated until finally, the output neuron that determines which character was read is activated.

See also:
* [Artificial Neural Network](https://en.wikipedia.org/wiki/Artificial_neural_network) from Wikipedia
* [This video](https://www.youtube.com/watch?v=kNPGXgzxoHw) which describes XOR problem and its ANN solution

For a single layer neural network in R, there is a package called [`nnet`](https://cran.r-project.org/web/packages/nnet/nnet.pdf):

```R
library (nnet)

fit <- nnet(X1~. , data = genes, size = 2 , decay = 0.0001, maxit = 10000)

pred <- predict(fit , genes[,2:51])

```

For a multiple hidden layer neural network, we can use [`neuralnet`](https://cran.r-project.org/web/packages/neuralnet/neuralnet.pdf):

```R
library(neuralnet)

fit <- neuralnet(f, data=train, hidden=c(16, 8, 4, 2), learningrate=0.0001)

pred <- compute(fit, test[,2:51])
```

## k-Nearest Neighbors (k-NN)

The k-Nearest Neighbors algorithm (or k-NN for short) is a non-parametric method used for classification and regression. In both cases, the input consists of the k closest training examples in the feature space. The output depends on whether k-NN is used for classification or regression:

* In k-NN classification, the output is a class membership. An object is classified by a majority vote of its neighbors, with the object being assigned to the class most common among its k nearest neighbors (k is a positive integer, typically small). If k = 1, then the object is simply assigned to the class of that single nearest neighbor.
* In k-NN regression, the output is the property value for the object. This value is the average of the values of its k nearest neighbors.

The [`class`](https://cran.r-project.org/web/packages/class/class.pdf) package in R, contains some of the classification methods including `knn`:

```R
library(class)

k <- knn.cv(data[,2:51], data$X1, k=10)
``

## K-means

K-means is one of the simplest unsupervised learning algorithms that solve the well known clustering problem. The procedure follows a simple and easy way to classify a given data set through a certain number of clusters (assume k clusters) fixed a priori. The main idea is to define k centroids, one for each cluster. These centroids shoud be placed in a cunning way because of different location causes different result. So, the better choice is to place them as much as possible far away from each other. The next step is to take each point belonging to a given data set and associate it to the nearest centroid. When no point is pending, the first step is completed and an early groupage is done. At this point we need to re-calculate k new centroids as barycenters of the clusters resulting from the previous step. After we have these k new centroids, a new binding has to be done between the same data set points and the nearest new centroid. A loop has been generated. As a result of this loop we may notice that the k centroids change their location step by step until no more changes are done. In other words centroids do not move any more.
Finally, this algorithm aims at minimizing an objective function, in this case a squared error function. The objective function:

![](image009.gif)

See also:
* [This Lecture](https://www.coursera.org/learn/machine-learning/lecture/93VPG/k-means-algorithm) from [Machine Learning course](https://www.coursera.org/learn/machine-learning) in [Coursera.org](https://coursera.org) which is describing k-means

The [`stats`](https://stat.ethz.ch/R-manual/R-devel/library/stats/html/kmeans.html) package in R does k-means. You should specify the data and the number of centroids:

```R
library(stats)

result <- kmeans(data[,2:51], centers=2)

results
```

### limma Package


### [lumi Package](http://bioconductor.org/packages/release/bioc/html/lumi.html)
integrated solution for the Illumina microarray data analysis. It includes functions of Illumina BeadStudio (GenomeStudio) data input, quality control, BeadArray-specific variance stabilization, normalization and gene annotation at the probe level. It also includes the functions of processing Illumina methylation microarrays

### [minfi Package](http://bioconductor.org/packages/release/bioc/html/minfi.html)
Analyzing and visualizing Illumina's methylation array data.
