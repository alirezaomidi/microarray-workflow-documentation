# Part 3: Preprocessing

### GCRMA Probe Summarization

GCRMA is a method of converting .CEL files into expression set using the Robust Multi-array Average (RMA) with the help of probe sequence and with GC-content background correction.

The background correction  is  motivated  by  the  assumptions  that  observed  PM  and  MM  values  consist  of  optical noise, non-specific binding noise, and signal.

This algorithm performs three jobs:

* Background Correction
* Normalization
* Probe Summarization: Conversion of probe level values to probeset expression values in an outlier resistance manner.


#### [gcrma Package](http://bioconductor.org/packages/release/bioc/html/gcrma.html)

This package adjusts for background intensities in Affymetrix array data which include optical noise and non-specific binding (NSB). The main function `gcrma` converts background adjusted probe intensities (`AffyBatch` or `ProbeSet`) to expression measures (`ExpressionSet`) using the same normalization and summarization methods as rma (Robust Multiarray Average).

> `AffyBatch` is a class representation for Affymetrix GeneChip probe level data. The main component are the intensities from multiple arrays of the same CDF type.

```R
celfiles.gcrma <- gcrma(celfiles)
```

The above code will perform GCRMA on the cel data (`celfile`) which is of type `AffyBatch`. The result stored in `celfiles.gcrma` is an object of class `ExpressionSet`.

> `ExpressionSet` class is a class to contain high throughput expression level assays.  

#### [farms Package](https://www.bioconductor.org/packages/release/bioc/html/farms.html)

The `farms` package provides a new summarization algorithm called **FARMS** - Factor Analysis for Robust Microarray Summarization and a novel unsupervised feature selection criterion called I/NI-calls.

The summarization method is based on a factor analysis model for which a Bayesian Maximum
a Posteriori method optimizes the model parameters under the assumption of Gaussian measurement
noise. Thereafter, the RNA concentration is estimated from the model.

`farms` does not use background correction and uses either *quantile normalization* or *cyclic loess*. Neverthess any other `affy` preprocessing method can be applied as well. It does not apply *PM corrections* and uses PMs only.

```R
library(farms)
library(affydata)
data(Dilution)

eset <- qFarms(Dilution)  # Quickly crete an expression set objects

# Or we can use expFarms which accepts more options
eset <- expFarms(Dilution, bgcorrect.method = "rma",
    pmcorrect.method = "pmonly", normalize.method = "constant")
```

`eset` is an object of type `ExpressionSet` which can be analyzed in many *BioConductor* packages.


### Quality Control and Checking

Before we analyse the data it is imperative that we do some quality control checks to make sure there are no issues with the dataset. The first thing we can do is check the **effects of the GC-RMA normalisation**, by plotting a boxplot of probe intensities before and after normalisation.

#### affyPLM Package

Methods for fitting probe-level models.

```R
# load colour libraries
library(RColorBrewer)
# set colour palette
cols <- brewer.pal(8, "Set1")
# plot a boxplot of unnormalised intensity values
boxplot(celfiles, col=cols)
# plot a boxplot of normalised intensity values, affyPLM is required to interrogate celfiles.gcrma
library(affyPLM)
boxplot(celfiles.gcrma, col=cols)
# the boxplots are somewhat skewed by the normalisation algorithm
# and it is often more informative to look at density plots
# Plot a density vs log intensity histogram for the unnormalised data
hist(celfiles, col=cols)
# Plot a density vs log intensity histogram for the normalised data
hist(celfiles.gcrma, col=cols)

```

![](boxplot.png)
![](gcrma-boxplot.png)

![](hist.png)
![](gcrma-hist.png)

From these plots we can conclude that there are no major deviations amongst the 12 chips, and normalisation has brought the intensities from all of the chips into distributions with similar characteristics.


This package also visualizes statistical characteristics of CEL files.

* `celfiles.qc <- fitPLM(celfiles)`: Perform probe-level metric calculations on the CEL files. The result is an object of class PLMset.

> **PLMSet**: This is a class representation for Probe level Linear Models fitted to Affymetrix GeneChip probe level data.

* `image(celfiles.qc, which=1, add.legend=TRUE)`: Create an image of GSM524665.CEL.
 Result:

![](qc-image.png)


* `RLE(celfiles.qc, main="RLE")`: RLE (Relative Log Expression) plots should have values close to zero. GSM524665.CEL is an outlier.

![](rle.png)

* `NUSE(celfiles.qc, main="NUSE")`: We can also use NUSE (Normalized Unscaled Standard Errors). The median standard error should be 1 for most genes. GSM524665.CEL appears to be an outlier on this plot too

![](nuse.png)

Following commands result hierarchical clustering to find relationship between samples.

```R
eset <- exprs(celfiles.gcrma)
distance <- dist(t(eset),method="maximum")
clusters <- hclust(distance)
plot(clusters)
```
![](hclust.png)
