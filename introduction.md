# Part 1: Intro to Bioconductor

## Bioconductor

Bioconductor is an open source, open development software project to provide tools for the analysis and comprehension of high-throughput genomic data. It is based primarily on the R programming language.



## Installing Bioconductor

To install Bioconductor simply run following command in R-Shell

```R

## try http:// if https:// URLs are not supported
source("https://bioconductor.org/biocLite.R")
biocLite()

```

## Installing Bioconductor Packages

In order to install a package in bioconductor series, simply run following:

```R

library (biocInstaller)
biocLite("<the package you want>")

```

## Vignettes

Each Bioconductor package contains one or more vignettes, documents that provide a textual, task-oriented description of the package’s functionality. Vignettes come in several forms. Many are “HowTo”s that demonstrate how a particular task can be accomplished with that package’s software. Others provide a more thorough overview of the package or discuss general issues related to the package.

Also the Bioconductor project has developed a program of short courses on software and statistical methods for the analysis of genomic data. Courses have been given for audiences with backgrounds in either biology or statistics. All course materials (lectures and computer labs) are available on this site.

## Types of Data

The basic data types as output of microarray chip are CEL, CDF, CHP, BPMap and BAR. The details over these data types are given in further topics. Dependent on the package used, different intermediate data types may involve in the work flow. In order to find the name of the data type just run `class (<variable name>)` and then in order to find more details on that class type just use `?<classname>`. Here two common classes are described.

* AffyBatch:
This is a class representation for Affymetrix GeneChip probe level data. The main component are the intensities from multiple arrays of the same CDF type.

* ExpressionSet:
Container for high-throughput assays and experimental metadata. ExpressionSet requires a matrix named exprs as assayData member.

## Further Readings
