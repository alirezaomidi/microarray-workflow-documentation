# Part 1: Intro to Bioconductor

## Bioconductor

Bioconductor is an open source, open development software project to provide tools for the analysis and comprehension of high-throughput genomic data. It is based primarily on the R programming language.



## Installing Bioconductor

To install Bioconductor simply run following command in R-Shell

```R

## try http:// if https:// URLs are not supported
source("https://bioconductor.org/biocLite.R")
biocLite()

```

## Installing Bioconductor Packages

In order to install a package in bioconductor series, simply run following:

```R

library (biocInstaller)
biocLite("<the package you want>")

```

## Vignettes

Each Bioconductor package contains one or more vignettes, documents that provide a textual, task-oriented description of the package’s functionality. Vignettes come in several forms. Many are “HowTo”s that demonstrate how a particular task can be accomplished with that package’s software. Others provide a more thorough overview of the package or discuss general issues related to the package.

Also the Bioconductor project has developed a program of short courses on software and statistical methods for the analysis of genomic data. Courses have been given for audiences with backgrounds in either biology or statistics. All course materials (lectures and computer labs) are available on this site.

## Types of Data

The basic data types as output of microarray chip are CEL, CDF, CHP, BPMap and BAR. The details over these data types are given in further topics. Dependent on the package used, different intermediate data types may involve in the work flow. In order to find the name of the data type just run `class (<variable name>)` and then in order to find more details on that class type just use `?<classname>`. Here two common classes are described.

* AffyBatch:
This is a class representation for Affymetrix GeneChip probe level data. The main component are the intensities from multiple arrays of the same CDF type.

* ExpressionSet:
Container for high-throughput assays and experimental metadata. ExpressionSet requires a matrix named exprs as assayData member.

## Further Readings



# Part 2: Manipulating Micro-Array Files

## File Formats

For manipulating microarray data there are several file types:

##### CEL File
The CEL file stores the results of the intensity calculations on the pixel values of the DAT file. This includes an intensity value, standard deviation of the intensity, the number of pixels used to calculate the intensity value, a flag to indicate an outlier as calculated by the algorithm and a user defined flag indicating the feature should be excluded from future analysis.  

For more information view the link below:
http://media.affymetrix.com/support/developer/powertools/changelog/gcos-agcc/cel.html

##### CDF File
The CDF file describes the layout for an Affymetrix GeneChip array. An array may contain Expression, Genotyping, CustomSeq, Copy Number and/or Tag probe sets. All probe set names within an array are unique. Multiple copies of a probe set may exist on a single array as long as each copy has a unique name.

For more information view the link below:
http://media.affymetrix.com/support/developer/powertools/changelog/gcos-agcc/cdf.html

## Packages in Bioconductor

### [affxparser](http://bioconductor.org/packages/release/bioc/html/affxparser.html)

![affxparser](http://bioconductor.org/shields/years-in-bioc/affxparser.svg)

Package for parsing Affymetrix files (CDF, CEL, CHP, BPMAP, BAR).

##### Documentation
http://bioconductor.org/packages/release/bioc/manuals/affxparser/man/affxparser.pdf

##### Example

### [affyio](http://bioconductor.org/packages/release/bioc/html/affyio.html)

Routines for parsing Affymetrix data files based upon file format information. Primary focus is on accessing the CEL and CDF file formats.


### [AffymetrixDataTestFiles](http://bioconductor.org/packages/release/data/experiment/html/AffymetrixDataTestFiles.html)

This package contains annotation data files and sample data files of Affymetrix file formats. The files originate from the Affymetrix' Fusion SDK distribution and other official sources.

`check.cdf.type` Returns a string to show the format of the file if parsed by it.

`read.cdffile.list` eads the entire contents of a cdf file into an R list structure

`read.celfile` reads the entire contents of a CEL file into an R list structure

`read.celfile.header` reads some of the header information (appears before probe intensity data) from the
supplied cel file.

`read.celfile.probeintensity.matrices` This function reads PM, MM or both types of intensities into matrices. These matrices have  all theprobes for a probeset in adjacent rows

### [illuminaio](http://bioconductor.org/packages/release/bioc/html/illuminaio.html)

Parsing Illumina Microarray Output Files

Tools for parsing Illumina's microarray output files, including IDAT.



##### Example From Vignette

###### Latex Style
```R
BiocStyle::latex()
```

###### Loading and Exploring Data
```R
library(illuminaio)
library(IlluminaDataTestFiles)
idatFile <- system.file("extdata", "idat", "4343238080_A_Grn.idat",
                        package = "IlluminaDataTestFiles")
idat <- readIDAT(idatFile)                                 

```

```R
names(idat)

idatData <- idat$Quants    
head(idatData)

```

```R
###################################################
### readGenotyping
###################################################
genotypeIdatFile <- system.file("extdata", "idat", "5723646052_R02C02_Grn.idat",
                        package = "IlluminaDataTestFiles")
genotypeIdat <- readIDAT(genotypeIdatFile)
names(genotypeIdat)


###################################################
### printGenotypingQuants
###################################################
head(genotypeIdat$Quants)


```

```R

###################################################
### code chunk number 7: ImportGenomeStudio
###################################################
gsFile <- system.file("extdata", "gs", "4343238080_A_ProbeSummary.txt.gz",
                      package = "IlluminaDataTestFiles")
gStudio <- read.delim(gsFile, sep = "\t", header = TRUE)
idatData <- idatData[which(idatData[,"CodesBinData"] %in% gStudio[,"ProbeID"]),]
gStudio <- gStudio[match(idatData[,"CodesBinData"], gStudio[,"ProbeID"]),]


###################################################
### code chunk number 8: figureComparingValues
###################################################
par(mfrow = c(1,2))
plot(idatData[, "MeanBinData"], gStudio[, "X4343238080_A.AVG_Signal"],
     xlab = "illuminaio", ylab = "GenomeStudio")
identical(idatData[, "MeanBinData"], gStudio[, "X4343238080_A.AVG_Signal"])
hist(idatData[, "MeanBinData"]- gStudio[, "X4343238080_A.AVG_Signal"],
     breaks = 100, main = "", xlab = "Difference")


###################################################
### code chunk number 9: sessionInfo
###################################################
toLatex(sessionInfo())

```


# Part 3: Preprocessing

### GCRMA Probe Summarization

GCRMA is a method of converting .CEL files into expression set using the Robust Multi-array Average (RMA) with the help of probe sequence and with GC-content background correction.

The background correction  is  motivated  by  the  assumptions  that  observed  PM  and  MM  values  consist  of  optical noise, non-specific binding noise, and signal.

This algorithm performs three jobs:

* Background Correction
* Normalization
* Probe Summarization: Conversion of probe level values to probeset expression values in an outlier resistance manner.


#### [gcrma Package](http://bioconductor.org/packages/release/bioc/html/gcrma.html)

This package adjusts for background intensities in Affymetrix array data which include optical noise and non-specific binding (NSB). The main function `gcrma` converts background adjusted probe intensities (`AffyBatch` or `ProbeSet`) to expression measures (`ExpressionSet`) using the same normalization and summarization methods as rma (Robust Multiarray Average).

> `AffyBatch` is a class representation for Affymetrix GeneChip probe level data. The main component are the intensities from multiple arrays of the same CDF type.

```R
celfiles.gcrma <- gcrma(celfiles)
```

The above code will perform GCRMA on the cel data (`celfile`) which is of type `AffyBatch`. The result stored in `celfiles.gcrma` is an object of class `ExpressionSet`.

> `ExpressionSet` class is a class to contain high throughput expression level assays.  

#### [farms Package](https://www.bioconductor.org/packages/release/bioc/html/farms.html)

The `farms` package provides a new summarization algorithm called **FARMS** - Factor Analysis for Robust Microarray Summarization and a novel unsupervised feature selection criterion called I/NI-calls.

The summarization method is based on a factor analysis model for which a Bayesian Maximum
a Posteriori method optimizes the model parameters under the assumption of Gaussian measurement
noise. Thereafter, the RNA concentration is estimated from the model.

`farms` does not use background correction and uses either *quantile normalization* or *cyclic loess*. Neverthess any other `affy` preprocessing method can be applied as well. It does not apply *PM corrections* and uses PMs only.

```R
library(farms)
library(affydata)
data(Dilution)

eset <- qFarms(Dilution)  # Quickly crete an expression set objects

# Or we can use expFarms which accepts more options
eset <- expFarms(Dilution, bgcorrect.method = "rma",
    pmcorrect.method = "pmonly", normalize.method = "constant")
```

`eset` is an object of type `ExpressionSet` which can be analyzed in many *BioConductor* packages.


### Quality Control and Checking

Before we analyse the data it is imperative that we do some quality control checks to make sure there are no issues with the dataset. The first thing we can do is check the **effects of the GC-RMA normalisation**, by plotting a boxplot of probe intensities before and after normalisation.

#### affyPLM Package

Methods for fitting probe-level models.

```R
# load colour libraries
library(RColorBrewer)
# set colour palette
cols <- brewer.pal(8, "Set1")
# plot a boxplot of unnormalised intensity values
boxplot(celfiles, col=cols)
# plot a boxplot of normalised intensity values, affyPLM is required to interrogate celfiles.gcrma
library(affyPLM)
boxplot(celfiles.gcrma, col=cols)
# the boxplots are somewhat skewed by the normalisation algorithm
# and it is often more informative to look at density plots
# Plot a density vs log intensity histogram for the unnormalised data
hist(celfiles, col=cols)
# Plot a density vs log intensity histogram for the normalised data
hist(celfiles.gcrma, col=cols)

```

![](boxplot.png)
![](gcrma-boxplot.png)

![](hist.png)
![](gcrma-hist.png)

From these plots we can conclude that there are no major deviations amongst the 12 chips, and normalisation has brought the intensities from all of the chips into distributions with similar characteristics.


This package also visualizes statistical characteristics of CEL files.

* `celfiles.qc <- fitPLM(celfiles)`: Perform probe-level metric calculations on the CEL files. The result is an object of class PLMset.

> **PLMSet**: This is a class representation for Probe level Linear Models fitted to Affymetrix GeneChip probe level data.

* `image(celfiles.qc, which=1, add.legend=TRUE)`: Create an image of GSM524665.CEL.
 Result:

![](qc-image.png)


* `RLE(celfiles.qc, main="RLE")`: RLE (Relative Log Expression) plots should have values close to zero. GSM524665.CEL is an outlier.

![](rle.png)

* `NUSE(celfiles.qc, main="NUSE")`: We can also use NUSE (Normalized Unscaled Standard Errors). The median standard error should be 1 for most genes. GSM524665.CEL appears to be an outlier on this plot too

![](nuse.png)

Following commands result hierarchical clustering to find relationship between samples.

```R
eset <- exprs(celfiles.gcrma)
distance <- dist(t(eset),method="maximum")
clusters <- hclust(distance)
plot(clusters)
```
![](hclust.png)


# Part 4: Analysis

## Filtering Data

The first stage of analysis is to filter out **uninformative data such as control probesets and other internal controls** as well as removing genes with low variance, that would be unlikely to pass statistical tests for differential expression, or are expressed uniformly close to background detection levels. The modifiers to nsFilter below tell nsFilter not to remove probesets without Entrez Gene identifiers, or have duplicated Entrez Gene identifiers.

```R

celfiles.filtered <- nsFilter(celfiles.gcrma, require.entrez=FALSE, remove.dupEntrez=FALSE)

```

In order to find which data was thrown away and why we can run:

```R
celfiles.filtered$filter.log
```

You will see something like this:

```
$numLowVar
[1] 27307

$feature.exclude
[1] 62
```

This means From this we conclude 27,307 probesets have been removed for reasons of low variance, and 62 control probesets have been removed as well.



#### [affy Package](http://bioconductor.org/packages/release/bioc/html/affy.html)

![affy](http://bioconductor.org/shields/years-in-bioc/affy.svg)

The affy package is for data analysis and exploration of Affymetrix oligonucleotide array probe level data.  
The software utilities provided with the Affymetrix software suite summarizes the probe set intensities to form one expression measure for each gene. The expression measure is the data available for analysis.

Some of the features are:

- Convert probe level data to expression values
    - Read *CEL* file information
    - Expression measures i.e. *RMA*, *MAS 5.0*, *MBEI*
- Quality Control
    - *MA Plot*
    - *PM* Correct
    - *Histograms*, *Images* & *Boxplots*
    - *RNA Degradation* plots
- Normalization


#### [affyQCReport Package](http://bioconductor.org/packages/release/bioc/html/affyQCReport.html)

This package creates a QC report for an AffyBatch object. The report is intended to allow the user to quickly assess the quality of a set of arrays in an AffyBatch object.


### Finding Differentially Expressed Probesets

There are some packages for analyzing microarray data files like `limma` and `affy` that we will describe them below.
To make the data ready for them following processes are required:

```R

samples <- celfiles.gcrma$Target
samples
samples <- as.factor(samples)
# set up the experimental design
design <- model.matrix(~0 + samples)
colnames(design) <- c("choroid", "huvec", "iris", "retina")

```

then the `design` is

```
choroid huvec iris retina
1        0     0    1      0
2        0     0    0      1
3        0     0    0      1
4        0     0    1      0
5        0     0    0      1
6        0     0    1      0
7        1     0    0      0
8        1     0    0      0
9        1     0    0      0
10       0     1    0      0
11       0     1    0      0
12       0     1    0      0
attr(,"assign")
[1] 1 1 1 1
attr(,"contrasts")
attr(,"contrasts")$samples
[1] "contr.treatment"
```

### Analyzing Data

## Artificial Neural Network

From [Wikipedia](https://en.wikipedia.org/wiki/Artificial_neural_network):

An Artificial neural network (ANN) is a network inspired by biological neural networks (the central nervous systems of animals, in particular the brain) which are used to estimate or approximate functions that can depend on a large number of inputs that are generally unknown.

For example, a neural network for handwriting recognition is defined by a set of input neurons which may be activated by the pixels of an input image. After being weighted and transformed by a function (determined by the network's designer), the activations of these neurons are then passed on to other neurons. This process is repeated until finally, the output neuron that determines which character was read is activated.

See also:
* [Artificial Neural Network](https://en.wikipedia.org/wiki/Artificial_neural_network) from Wikipedia
* [This video](https://www.youtube.com/watch?v=kNPGXgzxoHw) which describes XOR problem and its ANN solution

For a single layer neural network in R, there is a package called [`nnet`](https://cran.r-project.org/web/packages/nnet/nnet.pdf):

```R
library (nnet)

fit <- nnet(X1~. , data = genes, size = 2 , decay = 0.0001, maxit = 10000)

pred <- predict(fit , genes[,2:51])

```

For a multiple hidden layer neural network, we can use [`neuralnet`](https://cran.r-project.org/web/packages/neuralnet/neuralnet.pdf):

```R
library(neuralnet)

fit <- neuralnet(f, data=train, hidden=c(16, 8, 4, 2), learningrate=0.0001)

pred <- compute(fit, test[,2:51])
```

## k-Nearest Neighbors (k-NN)

The k-Nearest Neighbors algorithm (or k-NN for short) is a non-parametric method used for classification and regression. In both cases, the input consists of the k closest training examples in the feature space. The output depends on whether k-NN is used for classification or regression:

* In k-NN classification, the output is a class membership. An object is classified by a majority vote of its neighbors, with the object being assigned to the class most common among its k nearest neighbors (k is a positive integer, typically small). If k = 1, then the object is simply assigned to the class of that single nearest neighbor.
* In k-NN regression, the output is the property value for the object. This value is the average of the values of its k nearest neighbors.

The [`class`](https://cran.r-project.org/web/packages/class/class.pdf) package in R, contains some of the classification methods including `knn`:

```R
library(class)

k <- knn.cv(data[,2:51], data$X1, k=10)
``

## K-means

K-means is one of the simplest unsupervised learning algorithms that solve the well known clustering problem. The procedure follows a simple and easy way to classify a given data set through a certain number of clusters (assume k clusters) fixed a priori. The main idea is to define k centroids, one for each cluster. These centroids shoud be placed in a cunning way because of different location causes different result. So, the better choice is to place them as much as possible far away from each other. The next step is to take each point belonging to a given data set and associate it to the nearest centroid. When no point is pending, the first step is completed and an early groupage is done. At this point we need to re-calculate k new centroids as barycenters of the clusters resulting from the previous step. After we have these k new centroids, a new binding has to be done between the same data set points and the nearest new centroid. A loop has been generated. As a result of this loop we may notice that the k centroids change their location step by step until no more changes are done. In other words centroids do not move any more.
Finally, this algorithm aims at minimizing an objective function, in this case a squared error function. The objective function:

![](image009.gif)

See also:
* [This Lecture](https://www.coursera.org/learn/machine-learning/lecture/93VPG/k-means-algorithm) from [Machine Learning course](https://www.coursera.org/learn/machine-learning) in [Coursera.org](https://coursera.org) which is describing k-means

The [`stats`](https://stat.ethz.ch/R-manual/R-devel/library/stats/html/kmeans.html) package in R does k-means. You should specify the data and the number of centroids:

```R
library(stats)

result <- kmeans(data[,2:51], centers=2)

results
```

### limma Package


### [lumi Package](http://bioconductor.org/packages/release/bioc/html/lumi.html)
integrated solution for the Illumina microarray data analysis. It includes functions of Illumina BeadStudio (GenomeStudio) data input, quality control, BeadArray-specific variance stabilization, normalization and gene annotation at the probe level. It also includes the functions of processing Illumina methylation microarrays

### [minfi Package](http://bioconductor.org/packages/release/bioc/html/minfi.html)
Analyzing and visualizing Illumina's methylation array data.

