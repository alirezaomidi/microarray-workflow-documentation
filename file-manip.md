# Part 2: Manipulating Micro-Array Files

## File Formats

For manipulating microarray data there are several file types:

##### CEL File
The CEL file stores the results of the intensity calculations on the pixel values of the DAT file. This includes an intensity value, standard deviation of the intensity, the number of pixels used to calculate the intensity value, a flag to indicate an outlier as calculated by the algorithm and a user defined flag indicating the feature should be excluded from future analysis.  

For more information view the link below:
http://media.affymetrix.com/support/developer/powertools/changelog/gcos-agcc/cel.html

##### CDF File
The CDF file describes the layout for an Affymetrix GeneChip array. An array may contain Expression, Genotyping, CustomSeq, Copy Number and/or Tag probe sets. All probe set names within an array are unique. Multiple copies of a probe set may exist on a single array as long as each copy has a unique name.

For more information view the link below:
http://media.affymetrix.com/support/developer/powertools/changelog/gcos-agcc/cdf.html

## Packages in Bioconductor

### [affxparser](http://bioconductor.org/packages/release/bioc/html/affxparser.html)

![affxparser](http://bioconductor.org/shields/years-in-bioc/affxparser.svg)

Package for parsing Affymetrix files (CDF, CEL, CHP, BPMAP, BAR).

##### Documentation
http://bioconductor.org/packages/release/bioc/manuals/affxparser/man/affxparser.pdf

##### Example

### [affyio](http://bioconductor.org/packages/release/bioc/html/affyio.html)

Routines for parsing Affymetrix data files based upon file format information. Primary focus is on accessing the CEL and CDF file formats.


### [AffymetrixDataTestFiles](http://bioconductor.org/packages/release/data/experiment/html/AffymetrixDataTestFiles.html)

This package contains annotation data files and sample data files of Affymetrix file formats. The files originate from the Affymetrix' Fusion SDK distribution and other official sources.

`check.cdf.type` Returns a string to show the format of the file if parsed by it.

`read.cdffile.list` eads the entire contents of a cdf file into an R list structure

`read.celfile` reads the entire contents of a CEL file into an R list structure

`read.celfile.header` reads some of the header information (appears before probe intensity data) from the
supplied cel file.

`read.celfile.probeintensity.matrices` This function reads PM, MM or both types of intensities into matrices. These matrices have  all theprobes for a probeset in adjacent rows

### [illuminaio](http://bioconductor.org/packages/release/bioc/html/illuminaio.html)

Parsing Illumina Microarray Output Files

Tools for parsing Illumina's microarray output files, including IDAT.



##### Example From Vignette

###### Latex Style
```R
BiocStyle::latex()
```

###### Loading and Exploring Data
```R
library(illuminaio)
library(IlluminaDataTestFiles)
idatFile <- system.file("extdata", "idat", "4343238080_A_Grn.idat",
                        package = "IlluminaDataTestFiles")
idat <- readIDAT(idatFile)                                 

```

```R
names(idat)

idatData <- idat$Quants    
head(idatData)

```

```R
###################################################
### readGenotyping
###################################################
genotypeIdatFile <- system.file("extdata", "idat", "5723646052_R02C02_Grn.idat",
                        package = "IlluminaDataTestFiles")
genotypeIdat <- readIDAT(genotypeIdatFile)
names(genotypeIdat)


###################################################
### printGenotypingQuants
###################################################
head(genotypeIdat$Quants)


```

```R

###################################################
### code chunk number 7: ImportGenomeStudio
###################################################
gsFile <- system.file("extdata", "gs", "4343238080_A_ProbeSummary.txt.gz",
                      package = "IlluminaDataTestFiles")
gStudio <- read.delim(gsFile, sep = "\t", header = TRUE)
idatData <- idatData[which(idatData[,"CodesBinData"] %in% gStudio[,"ProbeID"]),]
gStudio <- gStudio[match(idatData[,"CodesBinData"], gStudio[,"ProbeID"]),]


###################################################
### code chunk number 8: figureComparingValues
###################################################
par(mfrow = c(1,2))
plot(idatData[, "MeanBinData"], gStudio[, "X4343238080_A.AVG_Signal"],
     xlab = "illuminaio", ylab = "GenomeStudio")
identical(idatData[, "MeanBinData"], gStudio[, "X4343238080_A.AVG_Signal"])
hist(idatData[, "MeanBinData"]- gStudio[, "X4343238080_A.AVG_Signal"],
     breaks = 100, main = "", xlab = "Difference")


###################################################
### code chunk number 9: sessionInfo
###################################################
toLatex(sessionInfo())

```
